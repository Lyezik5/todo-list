import { AddTodoButton } from "./styles"

export const AddTodo = ({onToggleModal}) => {

    return (
        <AddTodoButton onClick={onToggleModal} >Add task</AddTodoButton>
    )
}
