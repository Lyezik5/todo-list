

export const TodoItem = ({ id, text, isDone, onToggleTodo, deadline, getDaysLeft }) => {
    const handleToggleTodo = () => {
        onToggleTodo?.(id)
    }
    console.log(getDaysLeft(deadline))

    return (
        <li>
            <input
                type='checkbox'
                checked={isDone}
                onChange={handleToggleTodo}
            />
            <span style={{ textDecoration: isDone ? 'line-through' : 'none' }}>{text}</span>
            <span>({getDaysLeft(deadline)} {getDaysLeft(deadline) === 1 || getDaysLeft(deadline) === -1 ? 'day' : 'days'} left)</span>
        </li>
    )
}
