import { TodoItem } from "../TodoItem"

export const TodoList = ({ title, items, onToggleTodo, getDaysLeft }) => {
  return (
    <>
      <h2>{title}</h2>
      <ul>
        {
          items.map(todo => (
            < TodoItem
              key={todo.id}
              {...todo}
              onToggleTodo= {onToggleTodo}
              getDaysLeft = {getDaysLeft}
            />
          ))
        }
      </ul>
    </>
  )
}
