import styled from "styled-components";

export const AddTaskModalWrapper = styled.div`
  position: absolute;
  padding: 50px;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
`;

export const AddTaskModalContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  background-color: rgb(120,120,120, 0.8);
`

export const CloseModalButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 40px;
  height: 40px;
`