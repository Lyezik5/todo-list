import { AddTaskModalWrapper, AddTaskModalContainer, CloseModalButton } from "./styles"
import { useEffect, useState } from "react";

export const AddTaskModal = ({ toggleModal, todos, setTodos }) => {

    const handleKeyPress = (event) => {
        const { keyCode } = event;
        if (keyCode === 27) {
            toggleModal();
        }
    }

    const HandleCloseModal = () => {
        toggleModal();
    }

    useEffect(() => {
        window.addEventListener('keydown', handleKeyPress);
        return () => {
            window.removeEventListener('keydown', handleKeyPress);
        }
    })

    const HandleAddTask = () => {
        console.log(todos)
        if (taskText && taskDate) {
            const addTask = [
                ...todos,
                {
                    id: todos.length + 1,
                    text: taskText,
                    isDone: false,
                    deadline: taskDate
                }
            ]
            setTodos(addTask);
        }
    }

    const [taskText, setTaskText] = useState('')
    const [taskDate, setTaskDate] = useState('')

    return (
        <AddTaskModalContainer onClick={HandleCloseModal}>
            <AddTaskModalWrapper onClick={(e) => e.stopPropagation()} >
                <h2>Add new task</h2>
                <input onChange={(event) => setTaskText(event.target.value)} />
                <input
                    type="date"
                    onChange={(event) => setTaskDate(event.target.value)} />
                <button onClick={HandleAddTask} >Add</button>
                <CloseModalButton onClick={HandleCloseModal} >X</CloseModalButton>
            </AddTaskModalWrapper>
        </AddTaskModalContainer>
    )
}
