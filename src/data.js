export const startTodoList = [
    { id: 1, text: 'Lye', isDone: true, deadline: '2024-06-30' },
    { id: 2, text: 'Dip', isDone: false, deadline: '2024-06-29' },
    { id: 3, text: 'Belchick', isDone: false, deadline: '2021-06-28' },
]