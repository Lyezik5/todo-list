import { useState } from "react";
import { TodoList } from "./components/TodoList";
import { startTodoList } from "./data";
import { AddTodo } from "./components/AddTodo";
import { AddTaskModal } from "./components/AddTaskModal";

function App() {

  const [todos, setTodos] = useState(startTodoList)

  const [showModal, setShowModal] = useState(false)

  const getOverdueTodos = () => {
    const today = new Date()
    return todos.filter((todo) => !todo.isDone && new Date(todo.deadline) < today)
  }

  const getActualTodos = () => {
    const today = new Date()
    return todos.filter((todo) => !todo.isDone && new Date(todo.deadline) >= today)
  }

  const getCompletedTodos = () => {
    return todos.filter((todo) => todo.isDone)
  }

  const getDaysLeft = (deadline) => {
    const today = new Date();
    const deadlineDate = new Date(deadline)
    return  Math.round((deadlineDate - today) / (1000 * 60 * 60 * 24));
  }

  const toggleTodo = (id) => {
    const updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        return { ...todo, isDone: !todo.isDone }
      } else {
        return todo
      }
    })
    setTodos(updatedTodos)
  }

  const toggleModal = () => {
    return setShowModal(!showModal)
  }

  return (
    <>
      <div>
        <h1>Todo List</h1>
        <AddTodo onToggleModal={toggleModal} />

        <TodoList
          title='Overdue'
          items={getOverdueTodos()}
          onToggleTodo={toggleTodo}
          getDaysLeft={getDaysLeft}
        />

        <TodoList
          title='Actual'
          items={getActualTodos()}
          onToggleTodo={toggleTodo}
          getDaysLeft={getDaysLeft}
        />

        <TodoList
          title='Completed'
          items={getCompletedTodos()}
          onToggleTodo={toggleTodo}
          getDaysLeft={getDaysLeft}
        />
      </div>
      {
        showModal &&
        <AddTaskModal
          toggleModal={toggleModal}
          todos={todos}
          setTodos={setTodos}
        />
      }
    </>
  );
}

export default App;
